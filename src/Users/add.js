import React, { Component } from 'react';

class AddUsers extends Component {
  render() {
    return (
      <div class="content-wrapper">
          <h3 class="page-heading mb-4">Add Users</h3>
          
          <div class="card-deck">
            <div class="card col-lg-12 px-0 mb-4">
            <div class="card-body">
                  <form class="forms-sample">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="email" class="form-control p-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control p-input" id="exampleInputPassword1" placeholder="Password" />
                    </div>
                    <div class="form-group">
                      <label for="exampleTextarea">Example textarea</label>
                      <textarea class="form-control p-input" id="exampleTextarea" rows="5" placeholder="Let us type some loremm ipsum...."></textarea>
                    </div>
                    <div class="form-group">
                      <div class="form-check">
                        <label>
                          <input type="checkbox" class="form-check-input" />
                          Check me out
                        <i class="input-helper"></i></label>
                      </div>
                      <div class="form-check disabled">
                        <label>
                          <input type="checkbox" disabled="" class="form-check-input" />
                          Disabled Checkbox
                        <i class="input-helper"></i></label>
                      </div>
                      <div class="form-radio">
                        <label>
                          <input name="sample" value="" type="radio" />
                          Radio option 1
                        <i class="input-helper"></i></label>
                      </div>
                      <div class="form-radio">
                        <label>
                          <input name="sample" value="" type="radio" />
                          Radio option 2
                        <i class="input-helper"></i></label>
                      </div>
                      <div class="form-radio disabled">
                        <label>
                          <input name="sample" value="" type="radio" disabled="" />
                          Disabled Radio option
                        <i class="input-helper"></i></label>
                      </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
            </div>
          </div>
        </div>
    );
  }
}

export default AddUsers;
