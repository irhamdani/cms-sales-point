import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import fakeAuth from '../fakeAuth';

class Login extends React.Component {
  state = {
    redirectToReferrer: false
  }
  login = () => {
    fakeAuth.authenticate(() => {
      this.setState(() => ({
        redirectToReferrer: true
      }))
    })
  }
  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    const { redirectToReferrer } = this.state

    if (redirectToReferrer === true) {
      return(
        <Redirect to={from} />
      )
     
      // console.log("hal=>", from);
      // console.log("stTE=>", this.state.redirectToReferrer)
    }

    return (
      <div class="container-scroller">
      <div class="container-fluid">
        <div class="row">
          <div class="content-wrapper full-page-wrapper d-flex align-items-center auth-pages">
            <div class="card col-lg-4 mx-auto">
              <div class="card-body px-5 py-5">
                <h3 class="card-title text-left mb-3">Login</h3>
                <form>
                    <div class="form-group">
                      <input type="text" class="form-control p_input" placeholder="Username" />
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control p_input" placeholder="Password" />
                    </div>
                    <div class="text-center">
                      <button type="button" onClick={this.login} class="btn btn-primary btn-block enter-btn">LOG IN</button>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    )
  }
}

export default Login;
