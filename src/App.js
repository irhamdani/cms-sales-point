import React, { Component } from 'react';
import {Switch, Route, Link, withRouter} from 'react-router-dom';
import Home from './Dashboard/';
import Users from './Users/';
import AddUsers from './Users/add';
import Login from './Login/';
import fakeAuth from './fakeAuth';
import PrivateRoute from './PrivateRoute';
import Protected from './Protected';

const AuthButton = withRouter(({ history }) => (
  fakeAuth.isAuthenticated ? (
    <p>
      Welcome! <button onClick={() => {
        fakeAuth.signout(() => history.push('/'))
      }}>Sign out</button>
    </p>
  ) : (
    <p>You are not logged in.</p>
  )
))

class App extends Component {
  render() {
    return (
    <div class="container-scroller">
      <nav class="navbar navbar-default col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="bg-white text-center navbar-brand-wrapper">
          <a class="navbar-brand brand-logo" href="index.html"><img src="images/logo_star_black.png" /></a>
          <a class="navbar-brand brand-logo-mini" href="index.html"><img src="images/logo_star_mini.jpg" alt="" /></a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
          <button class="navbar-toggler navbar-toggler d-none d-lg-block navbar-dark align-self-center mr-3" type="button" data-toggle="minimize">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </nav>

      <div class="container-fluid">
        <div class="row row-offcanvas row-offcanvas-right">
          <nav class="bg-white sidebar sidebar-offcanvas" id="sidebar">
            <div class="user-info">
              <img src="images/face.jpg" alt="" />
              <p class="name">Richard V.Welsh</p>
              <p class="designation">Manager</p>
              <span class="online"></span>
            </div>
            <ul class="nav">
              <li class="nav-item active">
                <Link class="nav-link" to='/'>
                  <img src="images/icons/1.png" alt="" />
                  <span class="menu-title">Dashboard</span>
                </Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to='/users'>
                  <img src="images/icons/1.png" alt="" />
                  <span class="menu-title">Users</span>
                </Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to='/login'>
                  <img src="images/icons/1.png" alt="" />
                  <span class="menu-title">Login</span>
                </Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to='/protected'>
                  <img src="images/icons/1.png" alt="" />
                  <span class="menu-title">protected</span>
                </Link>
              </li>
            </ul>
          </nav>

          <main role="main" className="col-lg-12" style={{ padding: '0'}}>
            <div style={{ marginTop: '0px', marginLeft:'800px' }}><AuthButton /></div>
            <Switch>
              <Route exact path={'/'} component={Home} />
              <Route path={'/users'} component={Users} />
              <Route path={'/usersadd'} component={AddUsers} />
              <Route path={'/login'} component={Login}/>
              <PrivateRoute path='/protected' component={Protected} />
            </Switch>
          </main>        

          <footer class="footer">
            <div class="container-fluid clearfix">
              <span class="float-right">
                  <a href="#">Star Admin</a> &copy; 2017
              </span>
            </div>
          </footer>
        </div>
      </div>
    </div>
    );
  }
}

export default App;
